package br.com.fortknox.streamCloud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@Slf4j
@EnableBinding(TesteLogProcessor.class)
public class Consumer {

    @StreamListener(target = TesteLogProcessor.INPUT)
    public void consume(String message) {
        log.info("recieved a string message : " + message);
    }
}
