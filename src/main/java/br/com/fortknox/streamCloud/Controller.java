package br.com.fortknox.streamCloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("teste")
public class Controller {

    @Autowired
    private TesteLogProcessor producer;


    @GetMapping
    public String sendStream(){
        producer.input().send(MessageBuilder.withPayload("Teste").build());
        return "Sucesso!";
    }
}
