package br.com.fortknox.streamCloud;

import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(TesteLogProcessor.class)
public class Producer {
    private TesteLogProcessor mySource;
    public Producer(TesteLogProcessor mySource) {
        super();
        this.mySource = mySource;
    }
    public TesteLogProcessor getMysource() {
        return mySource;
    }
    public void setMysource(TesteLogProcessor mysource) {
        mySource = mySource;
    }
}

