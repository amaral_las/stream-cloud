package br.com.fortknox.streamCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamCloudApplication {
	public static void main(String[] args) {
		SpringApplication.run(StreamCloudApplication.class, args);
	}
}
