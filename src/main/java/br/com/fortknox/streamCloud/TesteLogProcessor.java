package br.com.fortknox.streamCloud;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;


public interface TesteLogProcessor {
        String INPUT = "input-idiota-log";
        String OUTPUT = "output-idiota-log";

        @Input (INPUT)
        SubscribableChannel input();

        @Output(OUTPUT)
        MessageChannel output();

}