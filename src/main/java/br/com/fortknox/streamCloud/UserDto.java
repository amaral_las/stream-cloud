package br.com.fortknox.streamCloud;

import lombok.Data;

@Data
public class UserDto {
    private String name;
    private Integer idade;

    public  UserDto(String n, Integer i){
        this.idade = i;
        this.name = n;
    }
}
